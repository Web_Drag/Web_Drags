<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLinks extends Model
{
    protected $table = "user_links";
    protected $fillable = ['type_id', 'link_id', 'deleted'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Couriers extends Model
{
    protected $table = "couriers";
    protected $fillable = ['image', 'name', 'surname', 'email', 'phone', 'deleted'];
}

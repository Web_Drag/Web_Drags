<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Users extends Model
{
    protected $table = "users";
    protected $fillable = ['name', 'surname', 'email', 'phone', 'username', 'password', 'priority', 'deleted'];


}

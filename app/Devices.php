<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    protected $table = "devices";
    protected $fillable = ['client_id', 'name', 'mac_address', 'description', 'deleted'];
}

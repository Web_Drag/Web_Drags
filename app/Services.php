<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = "services";
    protected $fillable = ['type_id', 'title', 'address', 'email', 'phone', 'deleted'];
}

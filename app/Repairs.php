<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repairs extends Model
{
    protected $table = "repairs";
    protected $fillable = ['device_id', 'repairman_id', 'deleted', 'status'];
}

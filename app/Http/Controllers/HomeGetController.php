<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Devices;
use App\Employees;
use App\Posts;
use App\Repairs;
use App\ServicesTypes;
use App\Statuses;
use Illuminate\Support\Facades\Session;

class HomeGetController extends HomeController
{
    public function get_index() {
        $types = ServicesTypes::where('deleted',0)->select()->get();
        return view('frontend.index')->with('types', $types);
    }

    public function get_client($link_id) {
        $client_id = Session::get('client_user_id');
        $devices = Devices::where(['client_id'=>$client_id, 'deleted'=>0])->select()->get();
        return view('frontend.client')->with(['link_id'=>$link_id, 'devices'=>$devices]);
    }

    //private
    public function get_customer_service($link_id) {
        $devices = Devices::leftJoin('clients as c', 'devices.client_id', '=', 'c.id')->where('devices.deleted',0)->orderBy('devices.id','DESC')->select('devices.*', 'c.name as c_name', 'c.surname as c_surname')->get();
        return view('frontend.service')->with(['link_id'=>$link_id, 'devices'=>$devices]);
    }

    public function get_customer_service_client_form($link_id) {
        return view('frontend.service_client_form')->with(['link_id'=>$link_id]);
    }

    public function get_customer_service_device_form($link_id) {
        $clients = Clients::where('deleted', 0)->select('id', 'name', 'surname')->get();
        return view('frontend.service_device_form')->with(['link_id'=>$link_id, 'clients'=>$clients]);
    }

    public function get_repair_service($link_id) {
        $user_id = Session::get('client_user_id');
//        $client_user_type = Session::get('client_user_type');

        $repairs = Repairs::leftJoin('devices as d', 'repairs.device_id', '=', 'd.id')->leftJoin('employees as e', 'repairs.repairman_id', '=', 'e.id')->where(['repairs.deleted'=>0, 'repairs.repairman_id'=>$user_id])->select('repairs.id as id', 'd.name as d_name', 'd.description', 'e.name as e_name', 'repairs.status', 'repairs.created_at', 'repairs.updated_at')->get();
        return view('frontend.repair_service')->with(['link_id'=>$link_id, 'repairs'=>$repairs]);
    }

    public function get_repair_service_form($link_id, $repair_id) {
        $statuses = Statuses::where('deleted', 0)->orderBy('status')->get();
        $repair = Repairs::where(['id'=>$repair_id, 'deleted'=>0])->select()->first();
        if (count($repair) == 0) {
            return redirect('/');
        }
        return view('frontend.repair_form')->with(['link_id'=>$link_id, 'statuses'=>$statuses, 'repair'=>$repair]);
    }

    public function get_device_repairman($device_id) {
//        $devices = Devices::where('deleted',0)->select()->get();
        $repairmen = Employees::leftJoin('services as s', 'employees.service_id', '=', 's.id')->where(['employees.deleted'=>0, 's.type_id'=>1])->select('employees.*')->get(); //duzelis edecem
        return view('frontend.device_repairman')->with(['device_id'=>$device_id, 'repairmen'=>$repairmen]);
    }
    //

    public function get_logout() {
        Session::forget('client_user_id');
        Session::forget('client_user_type');
        return redirect('/');
    }

    //client
    public function get_client_more_status($device_id, $link_id) {
        $posts = Posts::leftJoin('couriers as c', 'posts.courier_id', '=', 'c.id')->where(['posts.device_id'=>$device_id, 'posts.deleted'=>0])->select('c.name', 'c.surname', 'c.image', 'c.phone')->orderBy('posts.id', 'DESC')->first();
        if (count($posts) > 0) {
            $post_arr = $posts;
        }
        else {
            $post_arr = array();
        }

        $repairs = Repairs::leftJoin('employees as e', 'repairs.repairman_id', '=', 'e.id')->leftJoin('services as s', 'e.service_id', '=', 's.id')->where(['repairs.device_id'=>$device_id, 'repairs.deleted'=>0])->select('repairs.status', 'e.name', 'e.surname', 'e.phone', 's.title as service')->orderBy('repairs.id', 'DESC')->first();
        if (count($repairs) > 0) {
            $repairs_arr = $repairs;
        }
        else {
            $repairs_arr = array();
        }

        $devices = Devices::where(['id'=>$device_id, 'deleted'=>0])->select()->first();

        return view('frontend.client_more_status')->with(['link_id'=>$link_id, 'post'=>$post_arr, 'repair'=>$repairs_arr, 'device'=>$devices]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Employees;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    //admin
    public function get_admin_login() {
        return view('backend.login');
    }

    public function post_admin_login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:50',
            'password' => 'required|max:100'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $pass = sha1(substr(md5($request->password), 0, 5));
            $login = Users::where(['username'=>$request->username, 'password'=>$pass, 'deleted'=>0])->orderBy('id', 'DESC')->select('id')->first();
            if (count($login) > 0) {
                Session::put('user_id', $login->id);
//                if (isset($request->remember)) {
//                    cookie('user_id', 10, 360);
//                }
                return redirect('admin/');
            }
            else {
                return view('backend.login')->with('status', 1);
            }
        } catch (\Exception $e) {
            return view('backend.login')->with('status', 2);
        }
    }


    //others
    public function post_index_for_login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:100',
            'password' => 'required|max:100',
            'login_option' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        $login_option = $request->login_option;
        $email = $request->email;
        $password = sha1(substr(md5($request->password), 0, 5));
        unset($request['login_option']);

        if ($login_option == 0) {
            $user = Clients::where(['email'=>$email, 'password'=>$password, 'deleted'=>0])->select('id')->first();
        }
        else {
            $user = Employees::leftJoin('services as s', 'employees.service_id', '=', 's.id')->where(['employees.email'=>$email, 'employees.password'=>$password, 'employees.deleted'=>0, 's.type_id'=>$login_option])->select('employees.id', 's.type_id')->first();
        }

        if (count($user) > 0) {
            Session::put('client_user_id', $user->id);
            Session::put('client_user_type', $login_option);
//                if (isset($request->remember)) {
//                    cookie('user_id', 10, 360);
//                }
            return redirect('/');
        }
        else {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'User not found!']);
        }
    }
}

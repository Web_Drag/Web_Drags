<?php

namespace App\Http\Controllers;

use App\ServicesTypes;
use App\Users;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(Session::get('user_id') == NULL)
            {
                return Redirect::to('/adminlogin');
            }else{
                $user_id = Session::get('user_id');
                $admin = Users::where(['id'=>$user_id, 'deleted'=>0])->select('users.*')->first();
                $services_types = ServicesTypes::where('deleted', 0)->select('services_types.*')->get();
                view()->share(['admin'=>$admin, 'services_types'=>$services_types]);
                return $next($request);
            }
        });
    }
}

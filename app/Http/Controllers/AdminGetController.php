<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Couriers;
use App\Devices;
use App\Employees;
use App\Links;
use App\Posts;
use App\Repairs;
use App\Services;
use App\ServicesTypes;
use App\Statuses;
use App\UserLinks;
use Illuminate\Http\Request;
use Session;

class AdminGetController extends AdminController
{
    public function get_index()
    {
        return view('backend.index');
    }

    public function get_logout() {
        Session::forget('user_id');
        return redirect('admin/index');
    }

    //employees
    public function get_employees($slug) {
        $employees = Employees::leftJoin('services as s', 'employees.service_id', '=', 's.id')->leftJoin('services_types as t', 's.type_id', '=', 't.id')->where(['t.slug'=>$slug, 'employees.deleted'=>0])->orderBy('employees.id', 'DESC')->select('employees.id as id', 'employees.name as name', 'employees.surname as surname', 'employees.email as email', 'employees.first_pass', 'employees.phone as phone', 'employees.username as username', 'employees.created_at as created_at', 'employees.updated_at as updated_at', 's.title as service')->get();
        return view('backend.employees')->with(['slug'=>$slug, 'employees'=>$employees]);
    }

    public function get_add_employee($slug) {
        $services = Services::leftJoin('services_types as t', 'services.type_id', '=', 't.id')->where(['t.slug'=>$slug, 'services.deleted'=>0])->orderBy('services.title')->select('services.id as id', 'services.title as title')->get();
        return view('backend.employee_add')->with(['services'=>$services, 'slug'=>$slug]);
    }

    public function get_update_employee($slug, $id) {
        $services = Services::leftJoin('services_types as t', 'services.type_id', '=', 't.id')->where(['t.slug'=>$slug, 'services.deleted'=>0])->orderBy('services.title')->select('services.id as id', 'services.title as title')->get();
        $employee = Employees::where(['id'=>$id, 'deleted'=>0])->select('employees.*')->first();
        if (count($employee) == 0) {
            return redirect('admin/index');
        }
        return view('backend.employee_update')->with(['id'=>$id, 'employee'=>$employee, 'services'=>$services, 'slug'=>$slug]);
    }

    //services
    public function get_services() {
        $services = Services::leftJoin('services_types as t', 'services.type_id', '=', 't.id')->where(['services.deleted'=>0])->orderBy('services.id', 'DESC')->select('services.id as id', 'services.title as title', 't.title as service_type', 'services.email as email', 'services.address as address', 'services.phone as phone')->get();
        return view('backend.services')->with(['services'=>$services]);
    }

    public function get_add_service() {
        $services_types = ServicesTypes::where('deleted', 0)->select('id', 'title')->get();
        return view('backend.service_add')->with(['services_types'=>$services_types]);
    }

    public function get_update_service($id) {
        $services_types = ServicesTypes::where('deleted', 0)->select('id', 'title')->get();
        $service = Services::where(['id'=>$id, 'deleted'=>0])->select('services.*')->first();
        if (count($service) == 0) {
            return redirect('admin/index');
        }
        return view('backend.service_update')->with(['id'=>$id, 'service'=>$service, 'services_types'=>$services_types]);
    }

    //status
    public function get_statuses() {
        $statuses = Statuses::where('deleted', 0)->select('statuses.*')->get();
        return view('backend.statuses')->with(['statuses'=>$statuses]);
    }

    public function get_add_status() {
        return view('backend.status_add');
    }

    public function get_update_status($id) {
        $status = Statuses::where(['id'=>$id, 'deleted'=>0])->select('statuses.*')->first();
        if (count($status) == 0) {
            return redirect('admin/index');
        }
        return view('backend.status_update')->with(['id'=>$id, 'status'=>$status]);
    }

    //services types
    public function get_types() {
        $types = ServicesTypes::where('deleted', 0)->orderBy('id', 'DESC')->select('services_types.*')->get();
        return view('backend.services_types')->with('types', $types);
    }

    public function get_add_type() {
        return view('backend.service_type_add');
    }

    public function get_update_type($id) {
        $type = ServicesTypes::where(['deleted'=>0, 'id'=>$id])->select('services_types.*')->first();
        if (count($type) == 0) {
            return redirect('admin/index');
        }
        return view('backend.service_type_update')->with('type', $type);
    }

    //couriers
    public function get_couriers() {
        $couriers = Couriers::where('deleted', 0)->orderBy('id', 'DESC')->select('couriers.*')->get();
        return view('backend.couriers')->with('couriers', $couriers);
    }

    public function get_add_courier() {
        return view('backend.courier_add');
    }

    public function get_update_courier($id) {
        $courier = Couriers::where(['id'=>$id, 'deleted'=>0])->select('couriers.*')->first();
        if (count($courier) == 0) {
            return redirect('admin/index');
        }
        return view('backend.courier_update')->with(['courier'=>$courier, 'id'=>$id]);
    }

    //clients
    public function get_clients() {
        $clients = Clients::where('deleted', 0)->orderBy('id', 'DESC')->select('clients.*')->get();
        return view('backend.clients')->with('clients', $clients);
    }

    //repairs
    public function get_repairs() {
        $repairs = Repairs::leftJoin('devices as d', 'repairs.device_id', '=', 'd.id')->leftJoin('clients as c', 'd.client_id', '=', 'c.id')->leftJoin('employees as e', 'repairs.repairman_id', '=', 'e.id')->leftJoin('services as s', 'e.service_id', '=', 's.id')->where(['repairs.deleted'=>0, 'd.deleted'=>0])->orderBy('repairs.id', 'DESC')->select('repairs.id', 'c.name as client_name', 'c.surname as client_surname', 'd.name as device_name', 'd.mac_address', 'e.name as employee_name', 'e.surname as employee_surname', 's.title as service_name', 'repairs.status', 'repairs.created_at', 'repairs.updated_at')->get();
        return view('backend.repairs')->with('repairs', $repairs);
    }

    //posts
    public function get_posts() {
        $posts = Posts::leftJoin('devices as d', 'posts.device_id', '=', 'd.id')->leftJoin('couriers as c', 'posts.courier_id', '=', 'c.id')->where('posts.deleted', 0)->orderBy('posts.id', 'DESC')->select('posts.id', 'd.name as device_name', 'd.mac_address as mac_address', 'c.name as courier_name', 'c.surname as courier_surname', 'posts.created_at')->get();
        return view('backend.posts')->with('posts', $posts);
    }

    //links
    public function get_links() {
        $links = Links::where('deleted', 0)->orderBy('id', 'DESC')->select('links.*')->get();
        return view('backend.links')->with('links', $links);
    }

    public function get_add_link() {
        return view('backend.link_add');
    }

    public function get_update_link($id) {
        $link = Links::where(['id'=>$id, 'deleted'=>0])->select('links.*')->first();
        if (count($link) == 0) {
            return redirect('admin/index');
        }
        return view('backend.link_update')->with(['link'=>$link, 'id'=>$id]);
    }

    //user links
    public function get_user_links() {
        $links = UserLinks::leftJoin('services_types as t', 'user_links.type_id', '=', 't.id')->leftJoin('links as l', 'user_links.link_id', '=', 'l.id')->where('user_links.deleted', 0)->orderBy('user_links.id', 'DESC')->select('user_links.id', 't.title as service', 'l.title as link')->get();
        return view('backend.user_links')->with('links', $links);
    }

    public function get_add_user_link() {
        $links = Links::where('deleted',0)->orderBy('title')->select()->get();
        $services_types = ServicesTypes::where('deleted', 0)->orderBy('title')->select()->get();
        return view('backend.user_link_add')->with(['links'=>$links, 'services_types'=>$services_types]);
    }

}

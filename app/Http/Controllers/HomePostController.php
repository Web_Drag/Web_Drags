<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Devices;
use App\Repairs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomePostController extends HomeController
{
    public function post_customer_service_client_form(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|max:100|email',
            'phone' => 'required|max:13',
            'username' => 'required|max:50'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $pass = str_random(6);
            $password = sha1(substr(md5($pass), 0, 5));
            $request->merge(['deleted'=>0, 'password'=>$password]);

            $add = Clients::create($request->all());
            $email = $request['email'];
            $to = $request['name']." ".$request['surname'];
            $message = "
                Dear customers, the record resulted in success. 
                Your username: {$request->email}. 
                Your password: {$pass}.
            ";
            $title = 'Successfully registration!';

            if ($add) {
                app('App\Http\Controllers\MailController')->get_send($email, $to, $title, $message);
            }
            return response(['case' => 'success', 'title' => 'Success!', 'content' => "Client added! Client's password: ".$pass]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Client could not be added!']);
        }
    }

    public function post_customer_service_device_form(Request $request) {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'name' => 'required|max:100',
            'mac_address' => 'required|max:20',
            'description'=>'required|max:3000'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $request->merge(['deleted'=>0]);

            $add = Devices::create($request->all());

            if ($add) {
                $client = Clients::where('id',$request->client_id)->select()->first();
                $email = $client['email'];
                $to = $client['name']." ".$client['surname'];
                $message = "Dear customers, Your device has been accepted and you will get information about repair process via our website or email.";
                $title = 'Your device has been accepted!';
                app('App\Http\Controllers\MailController')->get_send($email, $to, $title, $message);
            }
            return response(['case' => 'success', 'title' => 'Success!', 'content' => "Device added!"]);
        }
        catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Device could not be added!']);
        }
    }

    public function post_device_repairman(Request $request, $device_id) {
        $validator = Validator::make($request->all(), [
            'repairman_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $request->merge(['deleted'=>0, 'status'=>'accepted', 'device_id'=>$device_id]);

            //mail gonder
//            if ($update) {
//                $email = $repair['email'];
//                $to = $repair['name']." ".$repair['surname'];
//                $message = "Dear customers, ".'status changed: '.$request->status.'!';;
//                $title = 'Status changed!';
//                app('App\Http\Controllers\MailController')->get_send($email, $to, $title, $message);
//            }

            Repairs::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => "Repair added!"]);
        }
        catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Repair could not be added!']);
        }
    }

    public function post_update_status(Request $request, $link_id, $repair_id) {
        $validator = Validator::make($request->all(), [
            'status' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $repair = Repairs::leftJoin('devices as d', 'repairs.device_id', '=', 'd.id')->leftJoin('clients as c', 'd.client_id', '=', 'c.id')->where(['repairs.id'=>$repair_id, 'repairs.deleted'=>0])->select('repairs.status', 'c.name', 'c.surname', 'c.email')->first();
            $status = $repair->status . '*' . $request->status;

            $update = Repairs::where(['id'=>$repair_id, 'deleted'=>0])->update(['status'=>$status]);

            if ($update) {
                $email = $repair['email'];
                $to = $repair['name']." ".$repair['surname'];
                $message = "Dear customers, ".'status changed: '.$request->status.'!';;
                $title = 'Status changed!';
                app('App\Http\Controllers\MailController')->get_send($email, $to, $title, $message);
            }

            return response(['case' => 'success', 'title' => 'Success!', 'content' => "Status chaneged!"]);
        }
        catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Status could not be chaneged!']);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Employees;
use App\UserLinks;
use Illuminate\Http\Request;
use App\ServicesTypes;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user_menu = '';
            if(Session::get('user_id') == NULL)
            {
                $types = ServicesTypes::where('deleted',0)->select()->get();
                return response()->view('frontend.index', ['types' => $types]);
            }else{
                $client_user_id = Session::get('client_user_id');
                $client_user_type = Session::get('client_user_type');
                if ($client_user_type == 0) {
                    $user = Clients::where(['id'=>$client_user_id, 'deleted'=>0])->select('clients.*')->first();
                    $user_menu = '';
                    $user_links = UserLinks::leftJoin('links as l', 'user_links.link_id', '=', 'l.id')->where(['user_links.deleted'=>0, 'user_links.type_id'=>$client_user_type])->select('l.title', 'l.link', 'user_links.link_id as link_id')->get();
                    foreach ($user_links as $user_link) {
                        $user_menu .= <<<HTML
                            <li><a href="{$user_link->link}/{$user_link->link_id}">{$user_link->title}</a></li>
HTML;
                    }
                }
                else {
                    $user = Employees::where(['id'=>$client_user_id, 'deleted'=>0])->select('employees.*')->first();
                    $user_menu = '';
                    $user_links = UserLinks::leftJoin('links as l', 'user_links.link_id', '=', 'l.id')->where(['user_links.deleted'=>0, 'user_links.type_id'=>$client_user_type])->select('l.title', 'l.link', 'user_links.link_id as link_id')->get();
                    foreach ($user_links as $user_link) {
                        $user_menu .= <<<HTML
                            <li><a href="{$user_link->link}/{$user_link->link_id}">{$user_link->title}</a></li>
HTML;
                    }
                }
                view()->share(['client_user_arr'=>$user, 'client_user_type'=>$client_user_type, 'user_menu'=>$user_menu]);
                return $next($request);
            }
        });
    }

    public static function private_pages($link_id, $client_user_arr, $client_user_type) {
        if (isset($client_user_type) && isset($client_user_arr) && count($client_user_arr)!=0) {
            $link = UserLinks::where(['link_id'=>$link_id, 'type_id'=>$client_user_type, 'deleted'=>0])->select('id')->get();
            if (count($link) != 0) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}

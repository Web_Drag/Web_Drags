<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Couriers;
use App\Employees;
use App\Links;
use App\Repairs;
use App\Services;
use App\ServicesTypes;
use App\Statuses;
use App\UserLinks;
use App\Users;
use Carbon\Carbon;
use phpDocumentor\Reflection\DocBlock\Tags\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Input;
use Image;


class AdminPostController extends AdminController
{
    //employees
    public function post_add_employee($slug, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:13',
            'username' => 'required|max:50',
            'service_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $pass = str_random(6);
            $password = sha1(substr(md5($pass), 0, 5));
            $request->merge(['deleted' => 0, 'password'=>$password, 'first_pass'=>$pass]);
            foreach ($request->input() as $k => $v) {
                if (!is_numeric($request[$k]) && empty($request[$k])) {
                    $request[$k] = "";
                }
            }

            Employees::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => "Employee added!"]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Employee could not be added!']);
        }
    }

    public function post_delete_employee(Request $request, $slug)
    {
        try {
            Employees::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Employee deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Employee could not be deleted!']);
        }
    }

    public function post_update_employee($slug, $id, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:13',
            'username' => 'required|max:50',
            'service_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            if (empty($request['password'])) {
                unset($request['password']);
            }
            else {
                if (strlen($request['password']) < 6) {
                    return response(['case' => 'error', 'title' => 'Error!', 'content' => 'The password must be at least 6 symbols!!!']);
                }
                $pass = sha1(substr(md5($request['password']), 0, 5));
                $request->merge(['password'=>$pass]);
            }

            unset($request['_token']);
            Employees::where('id', $id)->update($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Employee information updated!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Employee information could not be updated!']);
        }
    }

    //services
    public function post_delete_service(Request $request)
    {
        try {
            Services::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Service deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Service could not be deleted!']);
        }
    }

    public function post_add_service(Request $request) {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required',
            'title' => 'required|max:255',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:13',
            'address' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            foreach ($request->input() as $k => $v) {
                if (!is_numeric($request[$k]) && empty($request[$k])) {
                    $request[$k] = "";
                }
            }
            $request->merge(['deleted' => 0]);

            Services::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Service added!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Service could not be added!']);
        }
    }

    public function post_update_service($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required',
            'title' => 'required|max:255',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:13',
            'address' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            unset($request['_token']);
            Services::where('id', $id)->update($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Service updated!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Service could not be updated!']);
        }
    }

    //services types
    public function post_add_type(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:245'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            foreach ($request->input() as $k => $v) {
                if (!is_numeric($request[$k]) && empty($request[$k])) {
                    $request[$k] = "";
                }
            }

            $rand = str_random(10);
            $slug = str_slug($request->title).'-'.$rand;

            $request->merge(['deleted' => 0, 'slug'=>$slug]);

            ServicesTypes::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Service added!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Service could not be added!']);
        }
    }

    public function post_update_type($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:245'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            unset($request['_token']);
            ServicesTypes::where('id', $id)->update($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Service type updated!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Service type could not be updated!']);
        }
    }

    public function post_delete_type(Request $request)
    {
        try {
            ServicesTypes::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Service type deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Service type could not be deleted!']);
        }
    }

    //couriers
    public function post_add_courier(Request $request) {
        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:13',
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields or wrong format!!!']);
        }

        $image = Input::file('image');
        $image_ext = $image->getClientOriginalExtension();
        $image_name = 'image' . str_random(4) . '_' . time() . '.' . $image_ext;
        Storage::disk('uploads')->makeDirectory('couriers');
        Image::make($image->getRealPath())->resize(100,100)->save('uploads/couriers/'.$image_name);
        $image_address = '/uploads/couriers/'.$image_name;

        try {
            $request->merge(['deleted' => 0]);
            foreach ($request->input() as $k => $v) {
                if (!is_numeric($request[$k]) && empty($request[$k])) {
                    $request[$k] = "";
                }
            }

            $request = Input::except('image');
            $request['image'] = $image_address;
            Couriers::create($request);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Courier added!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Courier could not be added!']);
        }
    }

    public function post_update_courier($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:13',
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields or wrong format!!!']);
        }

        $image_address = '';
        $old_image = '';
        $old_image = $request->old_image;
        unset($request['old_image']);

        if (isset($request->image)) {
            $validator_image = Validator::make($request->all(), [
                'image' => 'mimes:jpeg,png,jpg,gif,svg',
            ]);
            if ($validator_image->fails()) {
                return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Image could not be uploaded! Wrong format!!!']);
            }

            $image = Input::file('image');
            $image_ext = $image->getClientOriginalExtension();
            $image_name = 'image' . str_random(4) . '_' . time() . '.' . $image_ext;
            Storage::disk('uploads')->makeDirectory('couriers');
            Image::make($image->getRealPath())->resize(100,100)->save('uploads/couriers/'.$image_name);
            $image_address = '/uploads/couriers/'.$image_name;
        }
        else {
            $image_address = $old_image;
        }
        try {
            $request = Input::except('image');
            $request['image'] = $image_address;
            unset($request['_token']);

            Couriers::where('id', $id)->update($request);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Courier information updated!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Courier information could not be updated!']);
        }
    }

    public function post_delete_courier(Request $request) {
        try {
            Couriers::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Courier information deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Courier information could not be deleted!']);
        }
    }

    //clients
    public function post_delete_client(Request $request) {
        try {
            Clients::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Client information deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Client information could not be deleted!']);
        }
    }

    //repairs
    public function post_delete_repair(Request $request) {
        try {
            Repairs::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Repairs deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Repairs could not be deleted!']);
        }
    }

    //links
    public function post_add_link(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:50',
            'link' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $request->merge(['deleted'=>0]);
            Links::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Link added!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Link could not be added!']);
        }
    }

    public function post_delete_link(Request $request) {
        try {
            Links::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Link deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Link could not be deleted!']);
        }
    }

    public function post_update_link($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:50',
            'link' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            unset($request['_token']);
            Links::where('id', $id)->update($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Link updated!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Link could not be updated!']);
        }
    }

    //user link
    public function post_add_user_link(Request $request) {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required',
            'link_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $request->merge(['deleted'=>0]);
            UserLinks::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'User Link added!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'User Link could not be added!']);
        }
    }

    public function post_delete_user_link(Request $request) {
        try {
            UserLinks::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'User Link deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'User Link could not be deleted!']);
        }
    }

    //statuses
    public function post_add_status(Request $request) {
        $validator = Validator::make($request->all(), [
            'status' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            $request->merge(['deleted'=>0]);
            Statuses::create($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Link added!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Link could not be added!']);
        }
    }

    public function post_delete_status(Request $request) {
        try {
            Statuses::where('id', $request->id)->update(['deleted' => 1]);
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Link deleted!', 'row_id' => $request->row_id]);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Link could not be deleted!']);
        }
    }

    public function post_update_status($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'status' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Fill required fields!!!']);
        }
        try {
            unset($request['_token']);
            Statuses::where('id', $id)->update($request->all());
            return response(['case' => 'success', 'title' => 'Success!', 'content' => 'Link updated!']);
        } catch (\Exception $e) {
            return response(['case' => 'error', 'title' => 'Error!', 'content' => 'Link could not be updated!']);
        }
    }
}

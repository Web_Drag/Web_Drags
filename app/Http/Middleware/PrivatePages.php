<?php

namespace App\Http\Middleware;

use Closure;

class PrivatePages
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($client_user_type) && isset($client_user_arr)) {
            //
        }
        return $next($request);
    }
}

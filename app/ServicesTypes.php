<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesTypes extends Model
{
    protected $table = "services_types";
    protected $fillable = ['title', 'slug', 'deleted'];

}

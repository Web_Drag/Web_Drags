<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = "employees";
    protected $fillable = ['name', 'surname', 'email', 'phone', 'username', 'password', 'first_pass', 'service_id', 'deleted'];
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//home site
Route::get('/', 'HomeGetController@get_index');
Route::get('/index', 'HomeGetController@get_index');
Route::get('/home', 'HomeGetController@get_index');
Route::get('/logout', 'HomeGetController@get_logout');
Route::get('/client/{link_id}', 'HomeGetController@get_client');
Route::get('/client/device/{device_id}/{link_id}', 'HomeGetController@get_client_more_status');

//Route::group(['prefix'=>'private', 'middleware'=>'PrivatePages'], function () {
//    Route::get('/customer-service/{link_id}', 'HomeGetController@get_customer_service');
//    Route::get('/customer-service/form/{link_id}', 'HomeGetController@get_customer_service_form');
//    Route::get('/repair-service/{link_id}', 'HomeGetController@get_repair_service');
//    Route::get('/repair-service/form/{link_id}', 'HomeGetController@get_repair_service_form');
//});

Route::get('/customer-service/{link_id}', 'HomeGetController@get_customer_service');
Route::get('/device-repairman/{device_id}', 'HomeGetController@get_device_repairman');
Route::post('/device-repairman/{device_id}', 'HomePostController@post_device_repairman');
Route::get('/customer-service/client/form/{link_id}', 'HomeGetController@get_customer_service_client_form');
Route::get('/customer-service/device/form/{link_id}', 'HomeGetController@get_customer_service_device_form');
Route::get('/repair-service/{link_id}', 'HomeGetController@get_repair_service');
Route::get('/repair-service/form/{link_id}/{repair_id}', 'HomeGetController@get_repair_service_form');
Route::post('/repair-service/form/{link_id}/{repair_id}', 'HomePostController@post_update_status');


Route::post('/', 'Controller@post_index_for_login');
Route::post('/index', 'Controller@post_index_for_login');
Route::post('/home', 'Controller@post_index_for_login');
Route::post('/customer-service/client/form/{link_id}', 'HomePostController@post_customer_service_client_form');
Route::post('/customer-service/device/form/{link_id}', 'HomePostController@post_customer_service_device_form');



//admin panel
Route::group(['prefix'=>'admin', 'middleware'=>'Admin'], function (){
    Route::get('/', 'AdminGetController@get_index');
    Route::get('/index', 'AdminGetController@get_index');
    Route::get('/home', 'AdminGetController@get_index');

    Route::get('/logout', 'AdminGetController@get_logout');

    Route::group(['prefix'=>'employees'], function () {
        Route::get('/{slug}', 'AdminGetController@get_employees');
        Route::post('/{slug}', 'AdminPostController@post_delete_employee');
        Route::get('/{slug}/update/{id}', 'AdminGetController@get_update_employee');
        Route::post('/{slug}/update/{id}', 'AdminPostController@post_update_employee');
        Route::get('/{slug}/add', 'AdminGetController@get_add_employee');
        Route::post('/{slug}/add', 'AdminPostController@post_add_employee');
    });

    Route::group(['prefix'=>'services'], function () {
        Route::get('/', 'AdminGetController@get_services');
        Route::post('/', 'AdminPostController@post_delete_service');
        Route::get('/add', 'AdminGetController@get_add_service');
        Route::post('/add', 'AdminPostController@post_add_service');
        Route::get('/update/{id}', 'AdminGetController@get_update_service');
        Route::post('/update/{id}', 'AdminPostController@post_update_service');
    });

    Route::group(['prefix'=>'statuses'], function () {
        Route::get('/', 'AdminGetController@get_statuses');
        Route::post('/', 'AdminPostController@post_delete_status');
        Route::get('/add', 'AdminGetController@get_add_status');
        Route::post('/add', 'AdminPostController@post_add_status');
        Route::get('/update/{id}', 'AdminGetController@get_update_status');
        Route::post('/update/{id}', 'AdminPostController@post_update_status');
    });

    Route::group(['prefix'=>'services-types'], function () {
        Route::get('/', 'AdminGetController@get_types');
        Route::post('/', 'AdminPostController@post_delete_type');
        Route::get('/add', 'AdminGetController@get_add_type');
        Route::post('/add', 'AdminPostController@post_add_type');
        Route::get('/update/{id}', 'AdminGetController@get_update_type');
        Route::post('/update/{id}', 'AdminPostController@post_update_type');
    });

    Route::group(['prefix'=>'couriers'], function () {
        Route::get('/', 'AdminGetController@get_couriers');
        Route::post('/', 'AdminPostController@post_delete_courier');
        Route::get('/add', 'AdminGetController@get_add_courier');
        Route::post('/add', 'AdminPostController@post_add_courier');
        Route::get('/update/{id}', 'AdminGetController@get_update_courier');
        Route::post('/update/{id}', 'AdminPostController@post_update_courier');
    });

    Route::group(['prefix'=>'links'], function () {
        Route::get('/', 'AdminGetController@get_links');
        Route::post('/', 'AdminPostController@post_delete_link');
        Route::get('/add', 'AdminGetController@get_add_link');
        Route::post('/add', 'AdminPostController@post_add_link');
        Route::get('/update/{id}', 'AdminGetController@get_update_link');
        Route::post('/update/{id}', 'AdminPostController@post_update_link');
    });

    Route::group(['prefix'=>'user-links'], function () {
        Route::get('/', 'AdminGetController@get_user_links');
        Route::post('/', 'AdminPostController@post_delete_user_link');
        Route::get('/add', 'AdminGetController@get_add_user_link');
        Route::post('/add', 'AdminPostController@post_add_user_link');
    });

    Route::get('/clients', 'AdminGetController@get_clients');
    Route::post('/clients', 'AdminPostController@post_delete_client');

    Route::get('/repairs', 'AdminGetController@get_repairs');
    Route::post('/repairs', 'AdminPostController@post_delete_repair');

    Route::get('/posts', 'AdminGetController@get_posts');
    Route::post('/posts', 'AdminPostController@post_delete_post');
});

//admin login
Route::get('/adminlogin', 'Controller@get_admin_login');
Route::post('/adminlogin', 'Controller@post_admin_login');

//mail
Route::get('/mail/send', 'MailController@get_send');
Route::post('/mail/send', 'MailController@get_send');

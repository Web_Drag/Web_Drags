@extends('backend.app')
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employees list</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Device name </th>
                                        <th class="column-title">MAC address </th>
                                        <th class="column-title">Client name </th>
                                        <th class="column-title">Repairman name </th>
                                        <th class="column-title">Service name </th>
                                        <th class="column-title">Last status </th>
                                        <th class="column-title">Created at </th>
                                        <th class="column-title">Updated at </th>
                                        <th class="column-title">Delete </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @php
                                        $row = 1;
                                    @endphp
                                    @foreach($repairs as $repair)
                                        @php
                                            $status = $repair->status;
                                            $status_arr = explode('*', $status);
                                            $last_status = $status_arr[count($status_arr)-1];
                                        @endphp
                                        <tr class="even pointer" id="row_{{$row}}">
                                            <td>{{$repair->id}}</td>
                                            <td>{{$repair->device_name}}</td>
                                            <td>{{$repair->mac_address}}</td>
                                            <td>{{$repair->client_name}} {{$repair->client_surname}}</td>
                                            <td>{{$repair->employee_name}} {{$repair->employee_surname}}</td>
                                            <td>{{$repair->service_name}}</td>
                                            <td>{{$last_status}}</td>
                                            <td>{{$repair->created_at}}</td>
                                            <td>{{$repair->updated_at}}</td>
                                            <td>
                                                <input class="btn btn-danger" type="button"
                                                       onclick="del(this, '{{$repair->id}}', '{{$row}}');"
                                                       value="Delete">
                                            </td>
                                        </tr>
                                        @php
                                            $row++;
                                        @endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/sweetalert2.min.css">
@endsection

@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/sweetalert2.min.js"></script>

    {{--alert--}}
    <script>
        function del(e, id, row_id) {
            swal({
                title: 'Are you sure you want to delete?',
                text: 'Erasing will be permanent',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancel',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete!'
            }).then(function (result) {
                if (result.value) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: "Post",
                        url: '',
                        data: {
                            'id': id,
                            '_token': CSRF_TOKEN,
                            'row_id': row_id
                        },
                        beforeSubmit: function () {
                            //loading
                            swal({
                                title: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                text: 'Loading, please wait...',
                                showConfirmButton: false
                            });
                        },
                        success: function (response) {
                            swal(
                                response.title,
                                response.content,
                                response.case
                            );
                            if (response.case === 'success') {
                                var elem = document.getElementById('row_' + response.row_id);
                                elem.parentNode.removeChild(elem);
                            }
                        }
                    });
                } else {
                    return false;
                }
            });
        }
    </script>
@endsection
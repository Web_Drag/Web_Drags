<?php
$alert = '';

if (isset($status)) {
    switch ($status) {
        case 1: $alert = <<<HTML
                <div class="alert alert-danger">
                  <strong>Ooops!</strong> User not found!
                </div>
HTML;
            break;
        case 2: $alert = <<<HTML
                <div class="alert alert-danger">
                  <strong>Error!</strong> Sorry! Something went wrong...
                </div>
HTML;
            break;
        default: $alert = '';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Azercell admin panel | Login</title>

    <!-- Bootstrap -->
    <link href="/backend/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/backend/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/backend/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="/backend/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/backend/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
<div>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form action="" method="post" id="form">
                    {{csrf_field()}}
                    <h1>Login</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="" name="username"/>
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="" name="password"/>
                    </div>
                    <div style="text-align: left;">
                        <input type="checkbox" name="remember" id="remember"/><label for="remember">&nbsp;&nbsp;Remember me</label>
                    </div>
                    <div>
                        <button class="btn btn-default submit" type="submit">Log in</button>
                        <a class="reset_pass" href="#">Lost your password?</a>
                    </div>
                </form>
                {!! $alert !!}
            </section>
        </div>
    </div>
</div>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Azercell admin panel</title>

    <!-- Bootstrap -->
    <link href="/backend/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/backend/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/backend/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/backend/build/css/custom.min.css" rel="stylesheet">

    @yield('css')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/admin/" class="site_title"><i class="fa fa-paw"></i> <span>Azercell</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic" style="padding: 15px;">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2 style="text-transform: capitalize;">{{$admin->name}} {{$admin->surname}}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="/admin/"><i class="fa fa-home"></i> Home page</a></li>
                            <li><a><i class="fa fa-users"></i> Employees <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    @foreach($services_types as $type)
                                    <li><a href="/admin/employees/{{$type->slug}}">{{$type->title}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a><i class="fa fa-wrench"></i> Services <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="/admin/services">Services</a></li>
                                    <li><a href="/admin/services-types">Services types</a></li>
                                </ul>
                            </li>
                            <li><a href="/admin/couriers"><i class="fa fa-male"></i> Couriers</a></li>
                            <li><a href="/admin/clients"><i class="fa fa-reddit-alien"></i> Clients</a></li>
                            <li><a href="/admin/statuses"><i class="fa fa-list-ul"></i> Statuses</a></li>
                            <li><a href="/admin/repairs"><i class="fa fa-laptop"></i> Repairs</a></li>
                            <li><a href="/admin/posts"><i class="fa fa-envelope"></i> Posts</a></li>
                            <li><a href="/admin/links"><i class="fa fa-link"></i> Links</a></li>
                            <li><a href="/admin/user-links"><i class="fa fa-anchor"></i> User links</a></li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span style="text-transform: capitalize;">{{$admin->name}} {{$admin->surname}}</span>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="/admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

    @yield('content')

    <!-- footer content -->
        <footer>
            <div class="pull-right">
                by <strong>Web's dragons</strong>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="/backend/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/backend/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/backend/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/backend/vendors/nprogress/nprogress.js"></script>

<!-- Custom Theme Scripts -->
<script src="/backend/build/js/custom.min.js"></script>

@yield('js')
</body>
</html>

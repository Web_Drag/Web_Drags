@extends('frontend.app')

<?php
use App\Http\Controllers\HomeController;
if (isset($client_user_type) && isset($client_user_arr) ) {
    $permit = HomeController::private_pages($link_id, $client_user_arr, $client_user_type);

    if ($permit==false) {
        Header("Location: /index");
        exit;
    }
}
else {
    Header("Location: /index");
    exit;
}
?>

@section('content')
    @include('frontend.menu')
    <div class="container add-form">
        <form class="well form-horizontal" action=" " method="post">
            {{csrf_field()}}
            <fieldset>
                <h3 style="text-align: center;">Add device</h3>
                <div class="form-group">
                    <label class="col-md-4 control-label">Client</label>
                    <div class="col-md-4 selectContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <select name="client_id" class="form-control selectpicker" >
                                <option>Client</option>
                                @foreach($clients as $client)
                                    <option value="{{$client->id}}">{{$client->name}} {{$client->surname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Text input-->

                <div class="form-group">
                    <label class="col-md-4 control-label">Device name</label>
                    <div class="col-md-4 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                            <input  name="name" placeholder="Device name" class="form-control"  type="text" required>
                        </div>
                    </div>
                </div>

                <!-- Text input-->

                <div class="form-group">
                    <label class="col-md-4 control-label" >MAC address</label>
                    <div class="col-md-4 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <input name="mac_address" placeholder="MAC address" class="form-control"  type="text" required>
                        </div>
                    </div>
                </div>

                <!-- Text input-->

                <div class="form-group">
                    <label class="col-md-4 control-label">Description</label>
                    <div class="col-md-4 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                            <textarea required name="description" class="form-control"></textarea>
                        </div>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">
                        <button type="submit" name="device" class="btn btn-warning" >Add <span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>

            </fieldset>
        </form>


    </div>
    </div><!-- /.container -->
@endsection

@section('css')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/frontend/css/services-form.css">

    <link rel="stylesheet" href="/css/sweetalert2.min.css">
@endsection

@section('js')
    <script src="/frontend/js/services-form.js"></script>

    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/sweetalert2.min.js"></script>

    <script>
        $(document).ready(function () {
            $('form').ajaxForm({
                beforeSubmit:function () {
                    //loading
                    swal ({
                        title: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                        text: 'Loading, please wait...',
                        showConfirmButton: false
                    });
                },
                success:function (response) {
                    swal(
                        response.title,
                        response.content,
                        response.case
                    );
                }
            });
        });
    </script>
@endsection
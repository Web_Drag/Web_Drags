@extends('frontend.app')

<?php
use App\Http\Controllers\HomeController;
if (isset($client_user_type) && isset($client_user_arr) ) {
    $permit = HomeController::private_pages($link_id, $client_user_arr, $client_user_type);

    if ($permit==false) {
        Header("Location: /index");
        exit;
    }
}
else {
    Header("Location: /index");
    exit;
}
?>

@section('content')
    @include('frontend.menu')

    <section class="table_user">
        <div class="container">

            <?php

                if (count($post) > 0) {
                    echo <<<HTML
                        <table>
                            <thead>
                            <tr>
                                <th>Courier name & surname</th>
                                <th>Courier image</th>
                                <th>Courier phone</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{$post->name} {$post->surname}</td>
                                    <td><img src="{$post->image}"></td>
                                    <td>{$post->phone}</td>
                                </tr>
                            </tbody>
                        </table>
HTML;
                }
                else if (count($repair) > 0) {
                    $status = explode('*', $repair->status);
                    $last_status = $status[count($status)-1];
                    echo <<<HTML
                        <table>
                            <thead>
                            <tr>
                                <th>Service</th>
                                <th>Repairman name & surname</th>
                                <th>Courier phone</th>
                                <th>Last status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{$repair->service}</td>
                                    <td>{$repair->name} {$repair->surname}</td>
                                    <td>{$repair->phone}</td>
                                    <td>{$last_status}</td>
                                </tr>
                            </tbody>
                        </table>
HTML;
                }
                else {
                    echo <<<HTML
                        <table>
                            <thead>
                            <tr>
                                <th>Device</th>=
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{$device->name}</td>
                                    <td>Waiting</td>
                                </tr>
                            </tbody>
                        </table>
HTML;
                }
            ?>

            {{--<table>--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>No</th>--}}
                    {{--<th>Status</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--</tbody>--}}
            {{--</table>--}}
        </div>
    </section>

    </div><!-- /.container -->
@endsection

@section('css')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/frontend/css/fix-form.css">

    {{--<link rel="stylesheet" href="/css/sweetalert2.min.css">--}}
@endsection

@section('js')
    <script src="/frontend/js/fix-form.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"></script>

    {{--<script src="/js/jquery.form.min.js"></script>--}}
    {{--<script src="/js/jquery.validate.min.js"></script>--}}
    {{--<script src="/js/sweetalert2.min.js"></script>--}}

    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('form').validate();--}}
            {{--$('form').ajaxForm({--}}
                {{--beforeSubmit:function () {--}}
                    {{--//loading--}}
                    {{--swal ({--}}
                        {{--title: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',--}}
                        {{--text: 'Loading, please wait...',--}}
                        {{--showConfirmButton: false--}}
                    {{--});--}}
                {{--},--}}
                {{--success:function (response) {--}}
                    {{--swal(--}}
                        {{--response.title,--}}
                        {{--response.content,--}}
                        {{--response.case--}}
                    {{--);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}


@endsection
@extends('frontend.app')

<?php
use App\Http\Controllers\HomeController;
if (isset($client_user_type) && isset($client_user_arr) ) {
    $permit = HomeController::private_pages($link_id, $client_user_arr, $client_user_type);

    if ($permit==false) {
        Header("Location: /index");
        exit;
    }
}
else {
    Header("Location: /index");
    exit;
}
?>

@section('content')
    @include('frontend.menu')
    <section class="table_user">
        <div class="container">
            <table id="customers">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Device name</th>
                    <th>Client full name</th>
                    <th>MAC address</th>
                    <th>Description</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Select repairman</th>
                </tr>
                </thead>
                <tbody>
                @foreach($devices as $device)
                <tr>
                    <td>{{$device->id}}</td>
                    <td>{{$device->name}}</td>
                    <td>{{$device->c_name}} {{$device->c_surname}}</td>
                    <td>{{$device->mac_address}}</td>
                    <td>{{$device->description}}</td>
                    <td>{{$device->created_at}}</td>
                    <td>{{$device->updated_at}}</td>
                    <td><a href="/device-repairman/{{$device->id}}" class="btn btn-primary">Select</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>

    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/frontend/css/services.css">
@endsection

@section('js')
    <script type="text/javascript" src="/frontend/js/services.js"></script>
@endsection
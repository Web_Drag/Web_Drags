@extends('frontend.app')
@section('content')
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand"><span>WEB`s</span> DRAGONS</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home" class="smoothScroll">Home</a></li>
                    <li><a href="#about" class="smoothScroll">About </a></li>
                    <li><a href="#contact" class="smoothScroll">Contact</a></li>

                    <?php
                        $signin = <<<HTML
                            <a href="#" data-toggle="modal" data-target="#modal2" class="wow fadeInUp section-btn btn btn-success smoothScroll" data-wow-delay="1s">SignIn</a>
HTML;
                        if (isset($client_user_type) && isset($client_user_arr)) {
                            $links = '';
                            if (isset($user_menu)) {
                                $links = $user_menu;
                                $signin = '';
                            }
                            echo <<<HTML
                                <li>
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span style="text-transform: capitalize;">{$client_user_arr['name']} {$client_user_arr['surname']}</span>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right drop-menu">
                                        {$links}
                                        <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
HTML;
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <section id="home" class="main">
        <div class="container">
            <div class="row">
                <div class="wow fadeInUp col-md-6 col-sm-5 col-xs-10 col-xs-offset-1 col-sm-offset-0" data-wow-delay="0.2s">
                    <img src="/frontend/img/home-img.png" class="img-responsive" alt="Home">
                </div>
                <div class="col-md-6 col-sm-7 col-xs-12">
                    <div class="home-thumb">
                        <h1 class="wow fadeInUp" data-wow-delay="0.6s">Thecnology Repair Services</h1>
                        <p class="wow fadeInUp" data-wow-delay="0.8s">Quick Mobile Help For All Kind of Phones</p>
                        {!! $signin !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="wow bounceIn section-title">
                        <h1>Our Branches</h1>
                    </div>
                </div>
                <div class="wow fadeInUp col-md-12 col-sm-12" data-wow-delay="0.4s">
                    <div class="slick">
                        <div class="card" >
                            <img class="card-img-top" src="/frontend/img/pic3.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Ehmedli</p>
                                <p class="card-text additional">+945845484</p>
                            </div>
                        </div>
                        <div class="card" >
                            <img class="card-img-top" src="/frontend/img/pic3.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">filial2</p>
                                <p class="card-text additional">+945845484</p>
                            </div>
                        </div>
                        <div class="card" >
                            <img class="card-img-top" src="/frontend/img/pic3.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">filial2</p>
                                <p class="card-text additional">+945845484</p>
                            </div>
                        </div>
                        <div class="card" >
                            <img class="card-img-top" src="/frontend/img/pic3.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">filial2</p>
                                <p class="card-text additional">+945845484</p>
                            </div>
                        </div>
                        <div class="card" >
                            <img class="card-img-top" src="/frontend/img/pic3.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">filial4</p>
                                <p class="card-text additional">+945845484</p>
                            </div>
                        </div>
                        <div class="card" >
                            <img class="card-img-top" src="/frontend/img/pic3.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">filial6</p>
                                <p class="card-text additional">+945845484</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <h1 class="wow fadeInUp" data-wow-delay="0.4s">Contact Us</h1>
                <div class="left-c col-md-6 col-sm-12">
                    <div data-wow-delay="0.8s" class="map wow fadeInDown">
                        <iframe
                                width="100%"
                                height="450"
                                frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDGePy_NvpCiRUHhYgQ0_i_t7PNzyTpCRA
    &q=Space+Needle,Seattle+WA" allowfullscreen>
                        </iframe>
                    </div>
                </div>
                <div class="wow fadeInUp right-c col-md-6 col-sm-12" >
                    <form action="/action_page.php">
                        <div class="form-group">
                            <label for="namec">Name:</label>
                            <input type="text" class="form-control" id="namec">
                        </div>
                        <div class="form-group">
                            <label for="mail">Email address:</label>
                            <input type="email" class="form-control" id="mail">
                        </div>
                        <div class="form-group">
                            <label for="sub">Subject:</label>
                            <input type="text" class="form-control" id="sub">
                        </div>
                        <div class="form-group">
                            <label for="msg">Message:</label>
                            <textarea  class="form-control" id="msg" name="" cols="30" rows="7"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    </div>

    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-popup">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title">Sign In</h2>
                </div>
                <form action="" method="post" id="form">
                    {{csrf_field()}}
                    <select name="login_option" class="modal-select form-control" required>
                        <option value="" disabled selected>Select your option</option>
                        <option value="0">Client</option>
                        @foreach($types as $type)
                            <option value="{{$type->id}}">{{$type->title}}</option>
                        @endforeach
                    </select>
                    <input name="email" type="text" class="form-control"  placeholder="E-mail" required>
                    <input name="password" type="password" class="form-control"  placeholder="Password" required>
                    <input name="submit" type="submit" class="form-control"  value="SignIn">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/frontend/css/main.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
@endsection

@section('js')
    <script type="text/javascript" src="/frontend/js/main.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script>
        $('.slick').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive:
                [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
        });
    </script>
@endsection
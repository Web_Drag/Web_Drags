<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand"><span>WEB`s</span> DRAGONS</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/#home" class="smoothScroll">Home</a></li>
                <li><a href="/#about" class="smoothScroll">About</a></li>
                <li><a href="/#contact" class="smoothScroll">Contact</a></li>
                <?php
                if (isset($client_user_type) && isset($client_user_arr)) {
                    $links = '';
                    if (isset($user_menu)) {
                        $links = $user_menu;
                    }
                    echo <<<HTML
                                <li class="user-menu" ">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span style="text-transform: capitalize;">{$client_user_arr['name']} {$client_user_arr['surname']}</span>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right drop-menu">
                                        {$links}
                                        <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
HTML;
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<style>
    .user-menu:hover, .user-menu:focus, .user-menu:active {
        background-color: #1c1c1c;
    }

    .user-menu a:hover, .user-menu a:focus, .user-menu a:active {
        background-color: #1c1c1c !important;
    }

    .user-menu ul:hover, .user-menu ul:focus, .user-menu ul:active, .user-menu ul {
        background-color: #1c1c1c !important;
    }
</style>
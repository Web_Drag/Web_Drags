<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Web's dragons</title>

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Unica+One' rel='stylesheet' type='text/css'>
    <style>
        .drop-menu li a {
            font-size: 14px !important;
        }
        .drop-menu li a i {
            font-size: 16px !important;
            padding: 17px;
        }
    </style>
    <!-- Main css -->
    @yield('css')
</head>
<body >
<!-- PRE LOADER -->
<div class="preloader">
    <div class="sk-spinner sk-spinner-pulse"></div>
</div>

<div class="wrapper">
    @yield('content')
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6">
                <div class="wow fadeInLeft footer-copyright" data-wow-delay="0.5s">
                    <p style="color: lightgoldenrodyellow">Copyright &copy; 2018 All rights reserved
                        <span>||</span>
                        <a href="https://plus.google.com/+templatemo" title="free css templates" target="_blank" style="color: lightgoldenrodyellow">Web's Dragons</a></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <ul class="wow fadeInRight social-icon" data-wow-delay="0.8s">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-google-plus"></a></li>
                    <li><a href="#" class="fa fa-dribbble"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Back top -->
<a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>
    @yield('js')
</body>
</html>
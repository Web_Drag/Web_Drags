@extends('frontend.app')

<?php
use App\Http\Controllers\HomeController;
if (isset($client_user_type) && isset($client_user_arr) ) {
    $permit = HomeController::private_pages($link_id, $client_user_arr, $client_user_type);

    if ($permit==false) {
        Header("Location: /index");
        exit;
    }
}
else {
    Header("Location: /index");
    exit;
}
?>

@section('content')
    @include('frontend.menu')
    <section class="table_user">
        <div class="container">
            <table id="customers">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Device name</th>
                    <th>Description</th>
                    <th>Last status</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Change status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($repairs as $repair)
                    @php
                    $status = $repair->status;
                    $status_arr = explode('*', $status);
                    $last_status = $status_arr[count($status_arr)-1];
                    @endphp
                <tr>
                    <td>{{$repair->id}}</td>
                    <td>{{$repair->d_name}}</td>
                    <td>{{$repair->description}}</td>
                    <td>{{$last_status}}</td>
                    <td>{{$repair->created_at}}</td>
                    <td>{{$repair->updated_at}}</td>
                    <td>
                        <a href="form/{{$link_id}}/{{$repair->id}}" class="btn btn-warning">Change</a>
                    </td>
                </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <h1 class="wow fadeInUp" data-wow-delay="0.4s">Contact Us</h1>
                <div class="left-c col-md-6 col-sm-12">
                    <div data-wow-delay="0.8s" class="map wow fadeInDown">
                        <iframe
                                width="100%"
                                height="450"
                                frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDGePy_NvpCiRUHhYgQ0_i_t7PNzyTpCRA
			    &q=Space+Needle,Seattle+WA" allowfullscreen>
                        </iframe>
                    </div>
                </div>
                <div class="wow fadeInUp right-c col-md-6 col-sm-12">
                    <form action="/action_page.php">
                        <div class="form-group">
                            <label for="namec">Name:</label>
                            <input type="text" class="form-control" id="namec">
                        </div>
                        <div class="form-group">
                            <label for="mail">Email address:</label>
                            <input type="email" class="form-control" id="mail">
                        </div>
                        <div class="form-group">
                            <label for="sub">Subject:</label>
                            <input type="text" class="form-control" id="sub">
                        </div>
                        <div class="form-group">
                            <label for="msg">Message:</label>
                            <textarea class="form-control" id="msg" name="" cols="30" rows="7"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/frontend/css/fix.css">
@endsection

@section('js')
    <script type="text/javascript" src="/frontend/js/fix.js"></script>
@endsection
@extends('frontend.app')

<?php
use App\Http\Controllers\HomeController;
if (isset($client_user_type) && isset($client_user_arr) ) {
    $permit = HomeController::private_pages($link_id, $client_user_arr, $client_user_type);

    if ($permit==false) {
        Header("Location: /index");
        exit;
    }
}
else {
    Header("Location: /index");
    exit;
}
?>

@section('content')
    @include('frontend.menu')
    <div class="container add-form">
        <form class="well form-horizontal" action=" " method="post"  id="contact_form form">
            {{csrf_field()}}
            <fieldset>
                <div class="form-group">
                    <label class="col-md-4 control-label">Status</label>
                    <div class="col-md-4 selectContainer" id="status">
                        <div class="input-group" id="select">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                            <select name="status" class="form-control selectpicker">
                                <option>Status</option>
                                @foreach($statuses as $status)
                                    <option value="{{$status->status}}">{{$status->status}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input class="btn btn-warning" type="button"
                           onclick="manuel(this);"
                           value="Manuel">
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-warning" >Edit <span class="glyphicon glyphicon-pencil"></span></button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

    <section class="table_user">
        <div class="container">
            <table>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php
                        $statuses = $repair->status;
                        $status_arr = explode('*', $statuses);
                        for ($i=0; $i<count($status_arr); $i++) {
                            $num = $i + 1;
                            echo <<<HTML
                                <tr>
                                    <td>{$num}</td>
                                    <td>{$status_arr[$i]}</td>
                                </tr>
HTML;
                        }
                ?>
                </tbody>
            </table>
        </div>
    </section>

    </div><!-- /.container -->
@endsection

@section('css')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/frontend/css/fix-form.css">

    <link rel="stylesheet" href="/css/sweetalert2.min.css">
@endsection

@section('js')
    <script src="/frontend/js/fix-form.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"></script>

    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/sweetalert2.min.js"></script>

    <script>
        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                beforeSubmit:function () {
                    //loading
                    swal ({
                        title: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                        text: 'Loading, please wait...',
                        showConfirmButton: false
                    });
                },
                success:function (response) {
                    swal(
                        response.title,
                        response.content,
                        response.case
                    );
                }
            });
        });
    </script>

    <script>
        function manuel(e) {
            var elem = document.getElementById('select');
            elem.parentNode.removeChild(elem);
            e.parentNode.removeChild(e);

            $('#status').html("<input class='form-control' name='status' placeholder='Status'>");
        }
    </script>


@endsection
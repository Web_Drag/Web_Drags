-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1:3306
-- Üretim Zamanı: 01 Haz 2018, 11:48:24
-- Sunucu sürümü: 5.5.53
-- PHP Sürümü: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `azercell`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `clients`
--

INSERT INTO `clients` (`id`, `name`, `surname`, `email`, `phone`, `username`, `password`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'Sahib', 'Fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', 'd3454ff09768662eb5b5445e59796b1edaa4192b', '2018-05-06 10:49:29', '2018-05-06 10:49:29', 1),
(2, 'Natiq', 'Ibisov', 'natiq@mail.ru', '089632545', 'natiq', 'b3733a14cc3f08d797b9879bd5cdffd2a59d758b', '2018-05-06 10:51:05', '2018-05-06 10:51:05', 1),
(4, 'dsfg', 'dfgf', 'sahib.fermanli@mail.ru', '0777220075', 'fsdf', 'e9e9cb6a8a7b283ba9bd21f8f965e2633cfbd96e', '2018-05-06 12:53:22', '2018-05-06 12:53:22', 1),
(5, 'sass', 'mass', 'sahib.fermanli@mail.ru', '0777220075', 'pass', 'd6adc3fd5b56b1f50a54c825099c38c7788c9e91', '2018-05-06 12:54:01', '2018-05-06 12:54:01', 1),
(6, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', '9cae7687b2320479d57c5fbe58ec727fd825b7f0', '2018-05-06 13:09:29', '2018-05-06 13:09:29', 1),
(7, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', '24cc9f62cc7d0129be5727b140eadcf0e662ed10', '2018-05-06 13:11:51', '2018-05-06 13:11:51', 1),
(8, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', 'fd8aa5d5733406be597879f6b49bf9df3392a1bd', '2018-05-06 13:12:58', '2018-05-06 13:12:58', 1),
(9, 'sahib', 'Fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', 'bae99b721df29588fe7914b80fbd11365dfd62d0', '2018-05-06 13:28:03', '2018-05-06 13:28:03', 1),
(10, 'yesfj', 'jenf', 'sahib.fermanli@mail.ru', '0777220075', 'fsdgfdsg', '6397a24357d342f414b76874dacc7522704c6e83', '2018-05-06 13:29:39', '2018-05-06 13:29:39', 1),
(11, 'sdjf', 'dfnmds', 'sahib.fermanli@mail.ru', '0777220075', 'dgfd', '859eafae9eb63516e8317212e7998988d0296849', '2018-05-06 13:30:20', '2018-05-06 13:30:20', 1),
(12, 'vdf', 'vmn', 'sahib.fermanli@mail.ru', '0777220075', 'fgf', 'd361bdb06cc80f65a05293825fa314d1a287312e', '2018-05-06 13:31:06', '2018-05-06 13:31:06', 1),
(13, 'sdbjf', 'bsdf', 'sahib.fermanli@mail.ru', '0777220075', 'fggd', '5a94e2fc31b4b5f68865b0fea861d345be7fd136', '2018-05-06 13:50:11', '2018-05-06 13:50:11', 1),
(14, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', 'b5e9519dcfb4a233f610b2fa592f9064b26e8674', '2018-05-06 14:07:42', '2018-05-06 14:07:42', 1),
(15, 'dfg', 'fg', 'sahib.fermanli@mail.ru', '0777220075', 'dsgf', '7f1f7c9d14e8cc246e8d4ba9273f24329237d440', '2018-05-06 14:08:44', '2018-05-06 14:08:44', 1),
(16, 'nmvs', 'dnvm', 'sahib.fermanli@mail.ru', '0777220075', 'nsdmv', '1647d501c3db0ae8a59703e81bba30ddeb9cc34c', '2018-05-06 14:09:42', '2018-05-06 14:09:42', 1),
(17, 'nmvs', 'dnvm', 'sahib.fermanli@mail.ru', '0777220075', 'nsdmv', '6064fb20399a91b148fd1a949b30c0de802820de', '2018-05-06 14:11:45', '2018-05-06 14:11:45', 1),
(18, 'nfmsef', 'bdf', 'sahib.fermanli@mail.ru', '0777220075', 'ndf', '4da85979fafdcf1de0863db2cd171be1a7126cde', '2018-05-06 14:11:58', '2018-05-06 14:11:58', 1),
(19, 'sndm', 'ndfm', 'sahib.fermanli@mail.ru', '0777220075', 'dsfds', '5794df6c1c348aace0b89250b9f1cfbc5145254d', '2018-05-06 14:12:59', '2018-05-06 14:12:59', 1),
(20, 'ndmvf', 'd v', 'sahib.fermanli@mail.ru', '0777220075', 'dfdfg', 'a6151aa9c6032beba9813d21e6403ebd3b7d72e0', '2018-05-06 14:30:43', '2018-05-06 14:30:43', 1),
(21, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib12', '827c99154cf112b048772fc4dc06080121f44ff9', '2018-05-06 14:38:43', '2018-05-06 14:38:43', 1),
(22, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'ddm', 'a9e18768de5e504e1171ec43e402972b086e2d77', '2018-05-06 14:39:27', '2018-05-06 14:39:27', 1),
(23, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'ddm', '6af77a8fd2320f207d27f194dba9327ad709ecbe', '2018-05-06 14:39:45', '2018-05-06 14:39:45', 1),
(24, 'sahib', 'fermnali', 'sahib.fermanli@mail.ru', '0777220075', 'sfndjs', '1abf0c7933ab6c381dd95e87e2e6656717937262', '2018-05-06 14:41:02', '2018-05-06 14:41:02', 1),
(25, 'sahib', 'fermnali', 'sahib.fermanli@mail.ru', '0777220075', 'sfndjs', '0a6697c2e0b67a404a645c2dd03f846e55afd981', '2018-05-06 14:46:55', '2018-05-06 14:46:55', 1),
(26, 'dvn', 'ndmf', 'sahib.fermanli@mail.ru', '0777220075', 'fdn', 'e66a60deaa3661aab5bf679c800408d49ccedfdc', '2018-05-06 14:47:13', '2018-05-06 14:47:13', 1),
(27, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sbfds', '9e5e98bf86d934c507cc46b504bb2a630dc29029', '2018-05-06 14:52:04', '2018-05-06 14:52:04', 1),
(28, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sbfds', '3b5633e590932498758b4c7d3e05659f11c61ac9', '2018-05-06 14:54:41', '2018-05-06 14:54:41', 1),
(29, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sbfds', '0cd5fd1afb0320c7652ab57ff30ab19b55e2676e', '2018-05-06 14:57:07', '2018-05-06 14:57:07', 1),
(30, 'sdf', 'snfdm', 'sahib.fermanli@mail.ru', '6523', 'ddsn', 'ba7d83c595f378dc7e4cf5e9c1d5dad6fc490189', '2018-05-06 14:57:23', '2018-05-06 14:57:23', 1),
(31, 'sdf', 'snfdm', 'sahib.fermanli@mail.ru', '6523', 'ddsn', '3fbaf1df002dcdc8cfa1b4f3aadfa1998601b7c6', '2018-05-06 14:58:13', '2018-05-06 14:58:13', 1),
(32, 'sdf', 'snfdm', 'sahib.fermanli@mail.ru', '6523', 'ddsn', 'f9855ccca95b14715fc3691cddff52813caa0b7e', '2018-05-06 14:59:51', '2018-05-06 14:59:51', 1),
(33, 'sdf', 'snfdm', 'sahib.fermanli@mail.ru', '6523', 'ddsn', '1349355f94ecb331e19cda89c1581330498341b5', '2018-05-06 15:02:52', '2018-05-06 15:02:52', 1),
(34, 'sahib', 'nsdf', 'sahib.fermanli@mail.ru', '0777220075', 'dfgs', '76b76ba663a5e2d69e88ae9b61742f4fea4ca6e8', '2018-05-06 15:04:49', '2018-05-06 15:04:49', 1),
(35, 'dfgd', 'dgd', 'sahib.fermanli@mail.ru', '0777220075', 'g', 'b39c502242816d87cf37eba7a1bca17907e2f4d1', '2018-05-06 15:14:12', '2018-05-06 15:14:12', 1),
(36, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sfds', 'f32596196b55389369551e7ec47fcdd333bb42fc', '2018-05-06 15:16:45', '2018-05-06 15:16:45', 1),
(37, 'sahib', 'fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib1907', '9793c5e42932fe976e59dceddcd01a87726670d1', '2018-05-06 15:19:49', '2018-05-06 15:19:49', 1),
(38, 'Nicat', 'Ismayilov', 'nicat@mail.ru', '215485', 'niko', '24069d38c7885ff127e7ed3c4d9eda42966fa410', '2018-05-06 15:43:08', '2018-05-06 15:43:08', 1),
(39, 'Sahib', 'Fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib', '29524130218a064a1353e41cb0febdfc9e31e27b', '2018-05-06 15:44:00', '2018-05-06 15:44:00', 1),
(40, 'Sahib', 'Fermanli', 'sahib.fermanli@mail.ru', '0777220075', 'sahib', '964fe889e00abc760271f19636b67a4cecb219a5', '2018-05-17 23:49:55', '2018-05-17 23:49:55', 1),
(41, 'Firudin', 'Fermanli', 'sahibfermanli230@mail.ru', '0777220075', 'firudin', 'aa29cce9e4335e56b543cacd945d14d1e843b019', '2018-05-18 03:35:06', '2018-05-18 03:35:06', 1),
(42, 'Nicat', 'Ismayilov', 'sahib.fermanli@mail.ru', '0515534105', 'niko', '79bb1d647dc861c31d1ba3d211387e4c74059394', '2018-05-18 07:05:31', '2018-05-18 07:05:31', 0),
(43, 'Memmed', 'memmedov', 'sahib.fermanli@mail.ru', '0515534105', 'memmed', '382fcacacaca49d6226ef9594cbc2fa919db3900', '2018-05-18 07:18:14', '2018-05-18 07:18:14', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `couriers`
--

CREATE TABLE `couriers` (
  `id` int(5) NOT NULL,
  `image` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `couriers`
--

INSERT INTO `couriers` (`id`, `image`, `name`, `surname`, `email`, `phone`, `created_at`, `updated_at`, `deleted`) VALUES
(1, '/uploads/couriers/imageQt3p_1525599544.jpg', 'Teymur', 'Agazade', 'teymur@mail.ru', '0751254789', '2018-05-06 09:39:05', '2018-05-06 09:39:05', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mac_address` varchar(17) NOT NULL,
  `description` varchar(3000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `devices`
--

INSERT INTO `devices` (`id`, `client_id`, `name`, `mac_address`, `description`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 'Honor 7x', '12:36:A2:25:BF:6D', '45sffdrf', '2018-05-06 11:11:53', '2018-05-06 11:11:53', 0),
(2, 1, 'Honor 7x', '12:36:A2:25:BF:6D', 'bnmdf b f', '2018-05-06 11:16:42', '2018-05-06 11:16:42', 0),
(3, 1, 'nsdkfmd', '12:36:A2:25:BF:6D', 'dfgddfgd', '2018-05-06 11:19:02', '2018-05-06 11:19:02', 0),
(4, 1, 'Honor 7x', '12:36:A2:25:BF:6D', 'sfgsdfgd', '2018-05-06 16:16:16', '2018-05-06 16:16:16', 0),
(5, 2, 'Xiomoi', '12:36:A2:25:BF:6D', 'dgnmdf', '2018-05-06 16:17:10', '2018-05-06 16:17:10', 0),
(6, 1, 'iphone 5s', '12:36:A2:25:BF:6D', 'dgshjbnf f', '2018-05-17 23:50:59', '2018-05-17 23:50:59', 0),
(7, 41, 'samsung galaxy a5', '12:36:A2:25:BF:6D', 'ekran xarabdir', '2018-05-18 03:35:56', '2018-05-18 03:35:56', 0),
(8, 42, 'galaxsy s4', '12:36:A2:25:BF:6D', 'geldi getdi', '2018-05-18 07:06:20', '2018-05-18 07:06:20', 0),
(9, 43, 'galaxsy s4', '12:36:A2:25:BF:6D', 'fsdbf', '2018-05-18 07:19:12', '2018-05-18 07:19:12', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_pass` varchar(100) NOT NULL,
  `service_id` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `employees`
--

INSERT INTO `employees` (`id`, `name`, `surname`, `email`, `phone`, `username`, `password`, `first_pass`, `service_id`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'Nicat', 'Ismayilli', 'nicat@mail.ru', '0775265965', 'niko', 'd3841a53bfe04e968d5d1ef0fe29d294453460b7', 'PrqUtx', 3, '2018-05-06 09:36:03', '2018-05-06 09:36:03', 0),
(2, 'Qismet', 'Memmedov', 'qismet@mail.ru', '025741258', 'qisi', 'f6a455cdcaaa0e3cd36922c9c71211ea4bdc958a', 'ewq2JC', 1, '2018-05-06 09:36:36', '2018-05-06 09:36:36', 0),
(3, 'ilkin', 'xelilov', 'ilkin@mail.ru', '255122', 'ilkin', '46222937d1462eb72444a1cf1b9a6100d279aa8e', '9Y6KES', 2, '2018-05-06 09:37:32', '2018-05-06 09:37:32', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `links`
--

CREATE TABLE `links` (
  `id` int(5) NOT NULL,
  `link` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `links`
--

INSERT INTO `links` (`id`, `link`, `title`, `created_at`, `updated_at`, `deleted`) VALUES
(1, '/customer-service', 'customer service', '2018-05-06 09:40:27', '2018-05-06 09:40:27', 0),
(2, '/customer-service/client/form', 'customer service client form', '2018-05-06 09:41:00', '2018-05-06 12:59:10', 0),
(3, '/repair-service', 'repair service', '2018-05-06 09:41:29', '2018-05-06 09:41:29', 0),
(4, '/repair-service/form', 'repair service form', '2018-05-06 09:41:51', '2018-05-06 09:41:51', 0),
(5, '/client', 'client', '2018-05-06 09:42:24', '2018-05-06 09:42:24', 0),
(6, '/customer-service/device/form', 'custom service device form', '2018-05-06 15:49:45', '2018-05-06 15:50:13', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `posts`
--

INSERT INTO `posts` (`id`, `device_id`, `courier_id`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 7, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `repairs`
--

CREATE TABLE `repairs` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `repairman_id` int(5) NOT NULL,
  `status` varchar(7000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `repairs`
--

INSERT INTO `repairs` (`id`, `device_id`, `repairman_id`, `status`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 3, 1, '', '2018-05-11 12:05:04', '2018-05-18 07:03:17', 1),
(2, 3, 1, 'accepted', '2018-05-11 12:08:43', '2018-05-18 07:03:12', 1),
(3, 5, 3, 'accepted*Accepted*Finish*Test edirem', '2018-05-11 12:09:14', '2018-05-18 07:03:08', 1),
(4, 3, 3, 'accepted*Finish*Finish*Finish*Finish*Finish*Finish*Finish*Accepted*manuel test*nicat', '2018-05-11 12:12:46', '2018-05-18 07:03:04', 1),
(5, 7, 2, 'accepted', '2018-05-18 03:37:33', '2018-05-18 07:03:00', 1),
(6, 7, 1, 'accepted*ekran duzeldildi', '2018-05-18 03:43:22', '2018-05-18 03:44:31', 1),
(7, 8, 1, 'accepted*status 1*alinmadi apar tulla*hjfbsdnf', '2018-05-18 07:08:21', '2018-05-18 07:10:00', 0),
(8, 9, 1, 'accepted*duzeltdim', '2018-05-18 07:20:08', '2018-05-18 07:21:49', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `services`
--

CREATE TABLE `services` (
  `id` int(5) NOT NULL,
  `type_id` int(3) NOT NULL,
  `title` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Tablo döküm verisi `services`
--

INSERT INTO `services` (`id`, `type_id`, `title`, `address`, `email`, `phone`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 2, 'Azercell m/x, 28 may', '28 may m/s', '28may@azercell.com', '051258741', '2018-05-06 09:33:37', '2018-05-06 09:33:37', 0),
(2, 1, 'Texno servis', 'nerimanov m/s', 'texno@mai.ru', '0128527414', '2018-05-06 09:34:18', '2018-05-06 09:34:18', 0),
(3, 1, '28deki padval', 'kucenin duneceyinden uzu sola donen kimi birinci padval', 'padval@da.com', '0521474789', '2018-05-06 09:35:17', '2018-05-06 09:35:17', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `services_types`
--

CREATE TABLE `services_types` (
  `id` int(3) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `services_types`
--

INSERT INTO `services_types` (`id`, `title`, `slug`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'Repair service', 'repair-service-nBxHvqCj0b', '2018-05-06 09:32:01', '2018-05-06 09:32:01', 0),
(2, 'Customer service', 'customer-service-wdGiUfVn5o', '2018-05-06 09:32:10', '2018-05-06 09:32:10', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `statuses`
--

CREATE TABLE `statuses` (
  `id` int(5) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `statuses`
--

INSERT INTO `statuses` (`id`, `status`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'Accepted', '2018-05-18 01:30:42', '2018-05-18 01:33:04', 0),
(2, 'Finish', '2018-05-18 01:33:24', '2018-05-18 01:33:24', 0),
(3, 'status 1', '2018-05-18 07:02:29', '2018-05-18 07:02:29', 0),
(4, 'status 2', '2018-05-18 07:02:37', '2018-05-18 07:02:37', 0),
(5, 'status 3', '2018-05-18 07:02:44', '2018-05-18 07:02:44', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `priority` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `username`, `password`, `email`, `priority`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'sahib', 'fermanli', 'sahib', 'e0bedd0f9127bd4e0ff208a6827cefc2bf620d7b', 'sahib.fermanli@mail.ru', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_links`
--

CREATE TABLE `user_links` (
  `id` int(5) NOT NULL,
  `type_id` int(5) NOT NULL,
  `link_id` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `user_links`
--

INSERT INTO `user_links` (`id`, `type_id`, `link_id`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 3, '2018-05-06 09:42:40', '2018-05-06 09:42:40', 0),
(2, 1, 4, '2018-05-06 09:42:49', '2018-05-18 06:58:27', 1),
(3, 2, 1, '2018-05-06 09:43:02', '2018-05-06 09:43:02', 0),
(4, 2, 2, '2018-05-06 09:43:12', '2018-05-06 09:43:12', 0),
(5, 0, 5, '2018-05-06 09:44:13', '2018-05-06 09:44:13', 0),
(6, 2, 6, '2018-05-06 15:50:29', '2018-05-06 15:50:29', 0);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Tablo için indeksler `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Tablo için indeksler `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `courier_id` (`courier_id`);

--
-- Tablo için indeksler `repairs`
--
ALTER TABLE `repairs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `repairman_id` (`repairman_id`);

--
-- Tablo için indeksler `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Tablo için indeksler `services_types`
--
ALTER TABLE `services_types`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `user_links`
--
ALTER TABLE `user_links`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- Tablo için AUTO_INCREMENT değeri `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Tablo için AUTO_INCREMENT değeri `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `links`
--
ALTER TABLE `links`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Tablo için AUTO_INCREMENT değeri `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `repairs`
--
ALTER TABLE `repairs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Tablo için AUTO_INCREMENT değeri `services`
--
ALTER TABLE `services`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `services_types`
--
ALTER TABLE `services_types`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `user_links`
--
ALTER TABLE `user_links`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `devices_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Tablo kısıtlamaları `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`);

--
-- Tablo kısıtlamaları `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`courier_id`) REFERENCES `couriers` (`id`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`);

--
-- Tablo kısıtlamaları `repairs`
--
ALTER TABLE `repairs`
  ADD CONSTRAINT `repairs_ibfk_1` FOREIGN KEY (`repairman_id`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `repairs_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`);

--
-- Tablo kısıtlamaları `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `services_types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
